/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.model;

import com.eir.ecom.db.enums.Status;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PreUpdate;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import org.hibernate.annotations.Type;
import org.json.JSONObject;

/**
 *
 * @author Andri D. Septian
 */
@Entity
@Table(name = "product_list")
public class Product implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "product_id_seq")
    @SequenceGenerator(name = "product_id_seq", sequenceName = "product_id_seq")
    @Column(name = "product_id", nullable = false)
    private Long productId;

    @Column(name = "cr_time", nullable = false)
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date crTime = new Date();

    @Column(name = "mod_time")
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date modTime;

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category categoryId;

    @Column(name = "product_name", nullable = false, unique = true)
    private String productName;

    @Column(name = "image")
    @Type(type = "text")
    private String image;

    @Column(name = "description")
    @Type(type = "text")
    private String description;

    @Column(name = "status", nullable = false)
    private Status.Data status = Status.Data.DRAFT;

    @Column(name = "view_count", nullable = false)
    private Integer viewCount = 0;

    @Column(name = "sold_count", nullable = false)
    private Integer soldCount = 0;

    @Column(name = "additionalData")
    @Type(type = "text")
    private String additionalData;

    public JSONObject getAdditionalData() {
        return new JSONObject(additionalData);
    }

    public void setAdditionalData(JSONObject additionalData) {
        this.additionalData = additionalData.toString();
    }


    @PreUpdate
    protected void onUpdate() {
        setModTime(new Date());
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public Date getCrTime() {
        return crTime;
    }

    public void setCrTime(Date crTime) {
        this.crTime = crTime;
    }

    public Date getModTime() {
        return modTime;
    }

    public void setModTime(Date modTime) {
        this.modTime = modTime;
    }

    public Category getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Category categoryId) {
        this.categoryId = categoryId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Status.Data getStatus() {
        return status;
    }

    public void setStatus(Status.Data status) {
        this.status = status;
    }

    public Integer getViewCount() {
        return viewCount;
    }

    public void setViewCount(Integer viewCount) {
        this.viewCount = viewCount;
    }

    public Integer getSoldCount() {
        return soldCount;
    }

    public void setSoldCount(Integer soldCount) {
        this.soldCount = soldCount;
    }
}
