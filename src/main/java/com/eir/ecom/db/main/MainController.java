/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.main;

import com.eir.ecom.db.model.ServiceDoc;
import com.eir.ecom.db.spring.SInit;
import java.text.ParseException;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author alex
 */
@RestController
public class MainController {

    @RequestMapping(value = "/insert/servicedoc", method = RequestMethod.POST)
    public String userRegistration(@RequestBody(required = false) String body,
            HttpServletRequest request) throws ParseException {

        JSONObject req = new JSONObject(body);

        System.out.println(req);

        String docname = req.getString("docname");
        String doccategory = req.getString("doccategory");
        String docvalue = req.getString("docvalue");
        String statusvalue = req.getString("status");

        try {
            ServiceDoc doc = new ServiceDoc();
            doc.setDocName(docname);
            doc.setDocCategory(doccategory);
            doc.setDocValue(docvalue);
            doc.setStatus(statusvalue);
            SInit.getServiceDocDao().saveorupdate(doc);
            return "saved";
        } catch (Exception e) {
            e.printStackTrace();
            return "failed to submit due to " + e.getMessage();
        }
    }

    @RequestMapping(value = "/get/apiflow", method = RequestMethod.POST)
    public String getApiFlow(@RequestBody(required = false) String body,
            HttpServletRequest request) throws ParseException {

        JSONObject req = new JSONObject(body);

        System.out.println(req);
        String doccategory = req.getString("doccategory");

        try {

            List<ServiceDoc> doc = (List<ServiceDoc>) SInit.getServiceDocDao().getEm()
                    .createQuery("SELECT a FROM ServiceDoc a WHERE a.docCategory =:docCategory")
                    .setParameter("docCategory", doccategory)
                    .getResultList();

            String result = "";

            for (ServiceDoc serviceDoc : doc) {
                result += serviceDoc.getDocValue();
            }

            System.out.println("Result : \n" + result);

            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return "failed to submit due to " + e.getMessage();
        }
    }
}
