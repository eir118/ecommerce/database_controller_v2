/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.Variation;
import java.util.List;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "VariationDao")
@Transactional
public class VariationDao extends Dao {
    
    public EntityManager getEm() {
        return em;
    }
    
    public Query Query = new Query();

    public Variation saveorupdate(Variation dat) {
        try {
            if (dat.getVariationId() == null) {
                em.persist(dat);
            } else {
                em.merge(dat);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return dat;
    }
    
    public List<Variation> findByProductId (Long productId){
        try {
            return (List<Variation>) em.createQuery("SELECT a FROM Variation a WHERE a.productId := productId")
                    .setParameter("productId", productId)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    
    public class Query {

        public Native Native = new Native();

        public Variation single(String query) {
            try {
                return (Variation) em.createQuery(query, Variation.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public List<Variation> list(String query) {
            try {
                return (List<Variation>) em.createQuery(query, Variation.class)
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public class Native {

            public Variation single(String query) {
                try {
                    return (Variation) em.createNativeQuery(query, Variation.class)
                            .setMaxResults(1)
                            .getSingleResult();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public List<Variation> list(String query) {
                try {
                    return (List<Variation>) em.createNativeQuery(query, Variation.class)
                            .getResultList();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}
