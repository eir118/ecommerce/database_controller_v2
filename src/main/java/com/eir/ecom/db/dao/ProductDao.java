/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.Product;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Andri D. Septian
 */
@Repository(value = "ProductDao")
@Transactional
public class ProductDao extends Dao {

    public EntityManager getEm() {
        return em;
    }
    
    Query Query = new Query();

    public Product saveorupdate(Product dat) {
        try {
            if (dat.getProductId() == null) {
                em.persist(dat);
            } else {
                em.merge(dat);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return dat;
    }

    public List<Product> findAll() {
        try {
            return (List<Product>) em.createQuery("SELECT a FROM Product a")
                    .getResultList();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Product findById(Long id) {
        try {
            return (Product) em.createQuery("SELECT a FROM Product a WHERE a.productId = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public Product findByCategory(Long id) {
        try {
            return (Product) em.createQuery("SELECT a FROM Product a WHERE a.categoryId = :id")
                    .setParameter("id", id)
                    .setMaxResults(1)
                    .getSingleResult();
        } catch (NoResultException nre) {
            return null;
        }
    }

    public class Query {

        Native Native = new Native();

        public Product single(String query) {
            try {
                return (Product) em.createQuery(query, Product.class)
                        .setMaxResults(1)
                        .getSingleResult();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public List<Product> list(String query) {
            try {
                return (List<Product>) em.createQuery(query, Product.class)
                        .getResultList();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        public class Native {

            public Product single(String query) {
                try {
                    return (Product) em.createNativeQuery(query, Product.class)
                            .setMaxResults(1)
                            .getSingleResult();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }

            public List<Product> list(String query) {
                try {
                    return (List<Product>) em.createNativeQuery(query, Product.class)
                            .getResultList();
                } catch (Exception e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
}
