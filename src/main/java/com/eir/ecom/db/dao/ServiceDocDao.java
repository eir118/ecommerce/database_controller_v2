/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eir.ecom.db.dao;

import com.eir.ecom.db.model.ServiceDoc;
import javax.persistence.EntityManager;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author alex
 */
@Repository(value = "ServiceDocDao")
@Transactional
public class ServiceDocDao extends Dao {

    public EntityManager getEm() {
        return em;
    }

    public ServiceDoc saveorupdate(ServiceDoc dat) {
        try {
            if (dat.getDocId() == null) {
                em.persist(dat);
            } else {
                em.merge(dat);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return dat;
    }

}
